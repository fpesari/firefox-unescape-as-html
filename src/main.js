'use strict';

// Author: Fabio Pesari
// SPDX-License-Identifier: AGPL-3.0-or-later

/* global browser */

browser.menus.create({
  id: 'unescape-as-html',
  title: 'Unescape text as HTML',
  contexts: ['editable']
});

browser.menus.onClicked.addListener(
  (info) => {
    if (info.menuItemId !== 'unescape-as-html') return;
    const code = `
    (function () {
      let target = browser.menus.getTargetElement(${info.targetElementId});
      let e = document.createElement('textarea');
      e.innerHTML = target.value;
      target.value = e.value;
    })();
    `;
    browser.tabs.executeScript({code: code});
  }
);
