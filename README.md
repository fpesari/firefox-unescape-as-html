# Unescape as HTML

Very simple extension for Mozilla Firefox (and compatible browsers) which
unescapes text in any editable input field as HTML, turning character
references (like `&pi;`) to Unicode characters (like `π`).

![Screenshot](screen.png)

## Usage

Right-click on any input field you have filled, select
`Unescape text as HTML` and the entered text should be replaced with an
unescaped version of it.

## Acknowledgements

Thanks to Rob Wu for this essential [JS trick](https://stackoverflow.com/questions/7394748/whats-the-right-way-to-decode-a-string-that-has-special-html-entities-in-it/7394787#7394787):
exactly what this extension needed!

And thanks to the [MDN](https://developer.mozilla.org/en-US/), of course!
Free knowledge is as important as free software.

## License

All files are released under the GNU Affero GPL, version 3 or any later
version.
